import React, {memo} from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import ComponentMapper from '../Components/ComponentMapper';

const GET_PAGE = gql`
  query Page($slug: String!) {
    page(slug: $slug) {
      id
      metaTitle
      metaDescription
      blocks {
        ... on HeroBlock {
          id
          type
        }
        ... on SubscribeBlock {
          id
          type
        }
      }
    }
  }
`;

const Page = (props) => {
  const { loading, error, data } = useQuery(GET_PAGE, {
    variables: {
      slug: props.location.pathname
    }
  });

  if (loading) return (<p>Loading</p>);

  if (error) return `Error! ${error}`;

  return (
    <main>
      <h1>Page Title: {data.page.metaTitle}</h1>
      <p>Page Description: {data.page.metaDescription}</p>
      <ul>
        {data.page.blocks.map(block => (
          <ComponentMapper key={block.id} {...block} />
        ))}
      </ul>
    </main>
  )
}

export default memo(Page);
