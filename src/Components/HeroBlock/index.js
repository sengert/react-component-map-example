import React, {memo} from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const GET_HERO = gql`
  query Block($id: ID!) {
    block(id: $id) {
      ... on HeroBlock {
        title,
        subtitle,
        featuredImage
      }
    }
  }
`;

const HeroBlock = (props) => {
  const {loading, error, data} = useQuery(GET_HERO, {
    variables: {
      id: props.id,
    }
  });

  if (loading) return <p>Loading...</p>
  if (error) return `Error: ${error}`;

  return (
    <div>
      <h2>{data.block.title}</h2>
      <img src={data.block.featuredImage} alt=""/>
    </div>
  )
}

export default memo(HeroBlock);
