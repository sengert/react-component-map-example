import React, {Suspense, memo} from 'react';

const components = {
  HeroBlock: React.lazy(() => import ('./HeroBlock')),
  SubscribeBlock: React.lazy(() => import ('./SubscribeBlock')),
  // HeroBlock: React.lazy(() => import ('./HeroBlock')),
  // SubscribeBlock: (<p>why not subscribe?</p>),
};

const ComponentMapper = (props) => {
  const TheComponent = components[props.type];
  return (
    <Suspense fallback={<p>The Component is loading...</p>}>
      <TheComponent {...props} />
    </Suspense>
  )
};

export default memo(ComponentMapper);
