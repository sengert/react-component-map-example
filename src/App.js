import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';

import PageScreen from './Screens/Page.screen';

const client = new ApolloClient({
  uri: 'https://graphql.scottengert.wtf/.netlify/functions/graphql'
});

function App() {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <div className="App">
          <header>
            <h1>This is a quick example of mapping components</h1>
            <ul>
              <li><Link to={'/'}>Home</Link></li>
              <li><Link to={'/somepage'}>Some Page</Link></li>
            </ul>
          </header>
            <Switch>
              <Route match={'*'} component={PageScreen} />
            </Switch>
        </div>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
